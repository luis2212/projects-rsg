import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Credentials } from '../interfaces/credential.interface';
import { Response } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class CredentialService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  createCredentials(credential: Credentials): Observable<Response> {
    return this.http.post<Response>(
      this.url + 'access-credentials',
      credential
    );
  }
  getOneCredential(id_credential: number) {
    return this.http.get<Credentials>(
      this.url + 'access-credentials/' + id_credential
    );
  }
  updateCredential(id_credential: number, credential: Credentials) {
    return this.http.put<Response>(
      this.url + 'access-credentials/' + id_credential,
      credential
    );
  }

  deleteCredental(id_credential: number) {
    return this.http.delete<Response>(
      this.url + 'access-credentials/' + id_credential
    );
  }
}
