import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MettingTest } from '../interfaces/mettingtest.interface';
import { Response } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class MettingService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  createMettingTest(metting: MettingTest): Observable<Response> {
    return this.http.post<Response>(this.url + 'metting_tests', metting);
  }

  getMettingById(id_metting: number) {
    return this.http.get<any>(this.url + 'metting_tests/' + id_metting);
  }

  updateMember(id_metting: number, body: any) {
    const data = {
      attendees: body
    }
    return this.http.put<Response>(this.url + 'metting_tests/' + id_metting,data);
  }

  updateMetting(id_metting: number, metting: MettingTest) {
    return this.http.put<Response>(
      this.url + 'metting_tests/' + id_metting,
      metting
    );
  }

  deleteMetting(id_metting: number) {
    return this.http.delete<Response>(this.url + 'metting_tests/' + id_metting);
  }
}
