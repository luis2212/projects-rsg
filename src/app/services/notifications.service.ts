import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  getNotifications(id_user: number) {
    return this.http.get<any>(this.url + 'notifications/user/' + id_user);
  }

  deleteAllNotifications(id_user:number) {
    return this.http.delete<any>(this.url + 'notifications/record/all/' + id_user);
  }

  deleteOneNotification(id_user:number){
    return this.http.delete<any>(this.url + 'notifications/record/' + id_user);
  }
}
