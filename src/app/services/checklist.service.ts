import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Checklist } from '../interfaces/checklist.interface';
import { Response } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class ChecklistService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}
  createChecklist(checklist: Checklist) {
    return this.http.post<Response>(this.url + 'checklist', checklist);
  }

  getChecklistById(id_checklist: number) {
    return this.http.get<Checklist>(this.url + 'checklist/' + id_checklist);
  }
  deleteChecklist(id_checklist: number) {
    return this.http.delete<Response>(this.url + 'checklist/' + id_checklist);
  }

  createTask(task: Task) {
    return this.http.post<Response>(this.url + 'tasks/', task);
  }
  deleteTask(id_task: number) {
    return this.http.delete<Response>(this.url + 'tasks/' + id_task);
  }

  updateTask(id_task: number, task: Task) {
    return this.http.put<Response>(this.url + 'tasks/' + id_task, task);
  }
}
