/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ScrumServiceService } from './scrum-service.service';

describe('Service: ScrumService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScrumServiceService]
    });
  });

  it('should ...', inject([ScrumServiceService], (service: ScrumServiceService) => {
    expect(service).toBeTruthy();
  }));
});
