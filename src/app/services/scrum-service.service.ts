import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../interfaces/response.interface';
import { Scard } from '../interfaces/scard.interface';

@Injectable({
  providedIn: 'root',
})
export class ScrumServiceService {
  url: string = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      mode: 'no-cors',
    }),
  };
  constructor(private http: HttpClient) {}

  getScumById(id_scard: number): Observable<Scard> {
    return this.http.get<Scard>(this.url + 'scrum-card/' + id_scard);
  }

  saveScrum(body: Scard) {
    const data = {
      scard_tittle: body.scard_tittle,
      image_cover: body.image_cover,
      scard_description: body.scard_description,
      start_date: body.start_date,
      end_date: body.end_date,
      color: body.color,
      scard_status: body.scard_status,
      start_date_devop: body.start_date_devop,
      end_date_devop: body.end_date_devop,
      dedicated_time: body.dedicated_time,
      archived_scard: body.archived_scard,
      id_list: body.id_list,
      keppers: body.keppers,
    };
    return this.http.post<Response>(this.url + 'scrum-card', data);
  }

  updateScrum(body: Scard, id_scard: number) {
    const data = {
      scard_tittle: body.scard_tittle,
      image_cover: body.image_cover,
      scard_description: body.scard_description,
      start_date: body.start_date,
      end_date: body.end_date,
      color: body.color,
      scard_status: body.scard_status,
      start_date_devop: body.start_date_devop,
      end_date_devop: body.end_date_devop,
      dedicated_time: body.dedicated_time,
      archived_scard: body.archived_scard,
      id_list: body.id_list,
      keppers: body.keppers,
      labels: body.labels,
      links: body.links,
    };
    return this.http.put<Response>(this.url + 'scrum-card/' + id_scard, data);
  }
  updateOnlyKeppers(body: any, id_scard: number) {
    const data = {
      keppers: body,
    };
    return this.http.put<Response>(this.url + 'scrum-card/' + id_scard, data);
  }

  updateOnlyLabels(body:any, id_scard:number){
    const data = {
      labels: body,
    }
    return this.http.put<Response>(this.url + 'scrum-card/'+ id_scard, data);
  }

  deleteScrum(id_scard: number) {
    return this.http.delete<Response>(this.url + 'scrum-card/' + id_scard);
  }

  createLinkScard(link: any) {
    return this.http.post<Response>(this.url + 'link-scard/', link);
  }

  deleteLinkScard(id_link: number) {
    return this.http.delete<Response>(this.url + 'link-scard/' + id_link);
  }
}
