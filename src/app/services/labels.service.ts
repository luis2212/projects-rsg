import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Label } from '../interfaces/labels.interface';
import { Response } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class LabelsService {

  url: string = environment.baseUrl;

constructor(private http: HttpClient) { }
  createLabels(newLabel:Label){
    return this.http.post<Response>(this.url + 'labels',newLabel)
  }
  deleteLabels(id_label:number){
    return this.http.delete<Response>(this.url + 'labels/' + id_label);
  }

  getLabelbyId(id_label:number){
    return this.http.get<any>(this.url + 'labels/' + id_label);
  }
}
