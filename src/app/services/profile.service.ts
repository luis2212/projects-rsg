import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}

  getBoardToUser(id_user: number) {
    return this.http.get<any>(this.url + 'users/board/' + id_user);
  }

  getGroupToUser(id_user: number) {
    return this.http.get<any>(this.url + 'users/group/' + id_user);
  }

  getFavoriteBoardToUser(id_user: number) {
    return this.http.get<any>(this.url + 'favorite_boards/' + id_user);
  }

  getMettingsToUser(id_user: number) {
    return this.http.get<any>(this.url + 'metting_tests/user/' + id_user);
  }

  getTicketToUser(id_user: number) {
    return this.http.get<any>(this.url + 'ticket/user/' + id_user);
  }

  getScrumToUser(id_user: number) {
    return this.http.get<any>(this.url + 'scrum-card/user/' + id_user);
  }

  onAddBoardFavorite(data: any){
    return this.http.post<any>(this.url + 'favorite_boards/', data);
  }
  onDeleteBoardFavorite(data: any){
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: data
    };
    return this.http.delete<Response>(this.url + 'favorite_boards', options);
  }
}
