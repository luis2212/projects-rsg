import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../interfaces/response.interface';
import { Tickets } from '../interfaces/tickets.interface';

@Injectable({
  providedIn: 'root',
})
export class TicketService {
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) {}
  createTicket(ticket: Tickets): Observable<Response> {
    return this.http.post<Response>(this.url + 'ticket', ticket);
  }
  getTicketById(id_ticket: number) {
    return this.http.get<any>(this.url + 'ticket/' + id_ticket);
  }
  updateTicket(id_ticket: number, ticket: Tickets) {
    return this.http.put<Response>(this.url + 'ticket/' + id_ticket, ticket);
  }

  deleteTicket(id_ticket: number) {
    return this.http.delete<Response>(this.url + 'ticket/' + id_ticket);
  }
}
