import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { DataGroup } from 'src/app/group/group.interface';
import { GroupService } from 'src/app/group/group.service';
import { Users } from 'src/app/users/user.interface';
import { UsersService } from 'src/app/users/users.service';
import { DialogData } from '../boards.component';
import { BoardsService } from '../boards.service';
import { ToastrService } from 'ngx-toastr';
import { Board } from '../board.interface';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-modal-update-board',
  templateUrl: './modal-update-board.component.html',
  styleUrls: ['./modal-update-board.component.scss']
})
export class ModalUpdateBoardComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private boardService: BoardsService,
    private userService: UsersService,
    private groupService: GroupService,
    @Inject(MAT_DIALOG_DATA) public data: Board,
    private toastr: ToastrService
  ) { }


  @ViewChild(BsDatepickerDirective, { static: false })
  datepicker: BsDatepickerDirective;
  registroBoard: FormGroup;
  subbmited = false;
  public startDate: Date;
  public endDate: Date;

  public users: Users[] = [];
  public dataGroup: DataGroup[] = [];

  public populateBoard: Board = this.data;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }
  ngOnInit() {
   
    //Llenar combo de usuarios
    this.userService.getUsers().subscribe((users: any) => {
      users.forEach((user: Users) => {
        this.users.push(user);
      })
    });
    //Llear combo de equipos
    this.groupService.getDataGroup().subscribe((dataGroup: any) => {
      dataGroup.forEach((dataG: DataGroup) => {
        this.dataGroup.push(dataG);

      })

    })

    //Inicializar formulario
    this.registroBoard = this.formBuilder.group({
      id_board: new FormControl('', Validators.required),
      board_name: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      description_board: new FormControl('', Validators.required),
      project_manager: new FormControl(0, Validators.required),
      team: new FormControl(0, Validators.required),
      board_finish: new FormControl('', Validators.required)
    });
    //Inicializar valores
    this.registroBoard.get('id_board').setValue(this.populateBoard.id_board);
    this.registroBoard.get('board_name').setValue(this.populateBoard.board_name);
    this.registroBoard.get('start_date').setValue((this.populateBoard.start_date));
    this.registroBoard.get('end_date').setValue((this.populateBoard.end_date));
    this.registroBoard.get('description_board').setValue(this.populateBoard.description_board);
    this.registroBoard.get('project_manager').setValue(this.populateBoard.users.id_user);
    this.registroBoard.get('team').setValue(this.populateBoard.group.datagroup_id);
    this.registroBoard.get('board_finish').setValue(this.populateBoard.board_finish)
  }
  convertDateFormat(date: string) {
    var info = date.split('-').reverse().join('/');
    return info;
  }

  onAlert() {
    this.toastr.success('Tablero actualizado', 'Toastr fun!');
  }

  onSubmit() {
    this.boardService.updateBoard(this.registroBoard.value).subscribe((res) => {
      
      if(!res.Error){
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Tablero Actualizado',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Ocurrio un error',
          showConfirmButton: false,
          timer: 1500
        })
      }
    });
  }
}
