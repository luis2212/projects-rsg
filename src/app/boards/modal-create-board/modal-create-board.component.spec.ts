/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { ModalCreateBoardComponent } from './modal-create-board.component';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

describe('ModalCreateBoardComponent', () => {
  let component: ModalCreateBoardComponent;
  let fixture: ComponentFixture<ModalCreateBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCreateBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
