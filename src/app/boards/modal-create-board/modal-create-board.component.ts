import {
  Component,
  HostListener,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  BsDatepickerDirective,
} from 'ngx-bootstrap/datepicker';

import { DataGroup } from 'src/app/group/group.interface';
import { GroupService } from 'src/app/group/group.service';
import { Users } from 'src/app/users/user.interface';
import { UsersService } from 'src/app/users/users.service';
import Swal from 'sweetalert2';
import { BoardsService } from '../boards.service';

@Component({
  selector: 'app-modal-create-board',
  templateUrl: './modal-create-board.component.html',
  styleUrls: ['./modal-create-board.component.scss'],
})
export class ModalCreateBoardComponent implements OnInit {
  @ViewChild(BsDatepickerDirective, { static: false })
  datepicker: BsDatepickerDirective;
  registroBoard: FormGroup;
  subbmited = false;
  public startDate: Date;
  public endDate: Date;

  public users: Users[] = [];
  public dataGroup: DataGroup[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private boardService: BoardsService,
    private userService: UsersService,
    private groupService: GroupService,
  ) { }


  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }
  ngOnInit() {
    //Llenar combo de usuarios
    this.userService.getUsers().subscribe((users: any) => {
      users.forEach((user: Users) => {
        this.users.push(user);
      })
    });
    //Llear combo de equipos
    this.groupService.getDataGroup().subscribe((dataGroup: any) => {
      dataGroup.forEach((dataG: DataGroup) => {
        this.dataGroup.push(dataG);
      })

    })

    //Inicializar formulario
    this.registroBoard = this.formBuilder.group({
      board_name: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      description_board: new FormControl('', Validators.required),
      project_manager: new FormControl(0, Validators.required),
      team: new FormControl(0, Validators.required),
    });
  }

  onSubmit() {
    
    this.boardService.saveBoard(this.registroBoard.value).subscribe((res) => {
      if(!res.Error){
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Tablero guardado',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Ocurrio un error',
          showConfirmButton: false,
          timer: 1500
        })
      }
      
    });
  }
}
