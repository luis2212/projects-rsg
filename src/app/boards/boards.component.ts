import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { BoardsService } from './boards.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalCreateBoardComponent } from './modal-create-board/modal-create-board.component';
import { Board } from './board.interface';
import { ModalUpdateBoardComponent } from './modal-update-board/modal-update-board.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Users } from '../users/user.interface';
import { ProfileService } from '../services/profile.service';

export interface DialogData {
  id_list: number;
  id_board: number;
  id_credential:number;
  id_scard: number;
  id_checklist: number;
  members: Users[];
}
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss'],
})
export class BoardsComponent implements OnInit {
  constructor(
    private boardsService: BoardsService,
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog,
    private ngxLoader: NgxUiLoaderService,
    public profileService: ProfileService
  ) {}

  public user: any;
  public id_user: number;
  public currentBoard: Board[] = [];
  public isFavorite: boolean = false;
  public favoriteBoards: Board[] = [];
  public id_favorites: number[] = [];

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.id_user = this.user.user.id_user;

    this.ngxLoader.start();
    this.getBoards();
  }

  getBoards() {
    this.id_favorites = [];
    this.profileService
      .getFavoriteBoardToUser(this.id_user)
      .subscribe((res) => {
        this.favoriteBoards = res.Favorite_Boards;
        this.favoriteBoards.forEach((favorite) => {
          this.id_favorites.push(favorite.id_board);
        });
      });

    // this.profileService.getBoardToUser(this.id_user).subscribe((res) => {
    //   this.currentBoard = res.Allboards;

    //   if (this.favoriteBoards.length > 0) {
    //     for (let i = 0; i < this.currentBoard.length; i++) {
    //       if (this.id_favorites.includes(this.currentBoard[i].id_board)) {
    //         this.currentBoard[i].isFavorite = true;
    //       } else {
    //         this.currentBoard[i].isFavorite = false;
    //       }
    //     }
    //   }

    //   this.ngxLoader.stop();
    // });

    this.boardsService.getBoards().subscribe((data) => {
      this.currentBoard = data;
      if (this.favoriteBoards.length > 0) {
        for (let i = 0; i < this.currentBoard.length; i++) {
          if (this.id_favorites.includes(this.currentBoard[i].id_board)) {
            this.currentBoard[i].isFavorite = true;
          } else {
            this.currentBoard[i].isFavorite = false;
          }
        }
      }
      this.ngxLoader.stop();
    });
  }
  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/');
  }
  //Abrir crear scrum
  openDialog() {
    const dialogRef = this.dialog.open(ModalCreateBoardComponent);

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.getBoards();
      }, 1000);
    });
  }
  openDialogEdit(board: Board) {
    const dialogRef = this.dialog.open(ModalUpdateBoardComponent, {
      data: board,
    });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.getBoards();
      }, 2000);
    });
  }
  openBoard() {
    this.router.navigateByUrl('/board');
  }
  deleteBoard(id_board: number) {
    Swal.fire({
      title: 'Estas seguro que quieres eliminar el tablero?',
      text: 'Esta acción es irreversible!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.boardsService.deleteBoard(id_board).subscribe((result) => {
          if (!result.Error) {
            Swal.fire(
              'Eliminado!',
              'Tablero se elimino exitosamente.',
              'success'
            );
            this.getBoards();
          } else {
            Swal.fire(
              'Ocurrio un error inesperado!',
              'Intentalo nuevamente',
              'error'
            );
          }
        });
      }
    });
  }
  onAddFavorite(id_board: number, isFavorite: boolean) {
    const data = {
      id_user: this.id_user,
      id_board: id_board,
    };
    console.log(isFavorite);
    if (isFavorite) {
      this.profileService.onDeleteBoardFavorite(data).subscribe((res) => {
        this.getBoards();
      });
    }
    this.profileService.onAddBoardFavorite(data).subscribe((res) => {
      this.getBoards();
    });

    this.isFavorite = !this.isFavorite;
  }
}
