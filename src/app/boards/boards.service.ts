import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Board } from './board.interface';
import { environment } from '../../environments/environment';
import { BoardList } from '../interfaces/board-list.interface';
import { Scard } from '../interfaces/scard.interface';
import { Response } from '../interfaces/response.interface';
import { MettingTest } from '../interfaces/mettingtest.interface';

@Injectable({
  providedIn: 'root',
})
export class BoardsService {
  constructor(private http: HttpClient) {}

  url: string = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      mode: 'no-cors',
    }),
  };

  //Metodos de HTTP BOARD
  getBoards(): Observable<any> {
    return this.http
      .get<any>(this.url + 'board', this.httpOptions)
      .pipe(catchError(this.handleError<any>('getBoards', [])));
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getBoardById(id: number): Observable<any> {
    return this.http.get<any>(this.url + 'board/' + id, this.httpOptions);
  }
  saveBoard(body: any): Observable<Response> {
    const data = {
      board_name: body.board_name,

      start_date: body.start_date,

      end_date: body.end_date,

      board_finish: false,

      description_board: body.description_board,

      archived_board: false,

      project_manager: parseInt(body.project_manager),

      team: parseInt(body.team),
    };

    return this.http
      .post<Response>(this.url + 'board', data)
      .pipe(
        catchError(this.handleError<any>('No se pudo guardar el tablero', []))
      );
  }

  updateBoard(body: Board): Observable<Response> {
    const data = {
      id_board: body.id_board,

      board_name: body.board_name,

      start_date: body.start_date,

      end_date: body.end_date,

      board_finish: body.board_finish,

      description_board: body.description_board,

      archived_board: false,

      project_manager: body.project_manager,

      team: body.team,
    };

    const updateUrl = this.url + 'board/' + data.id_board;

    return this.http
      .put<Board>(updateUrl, data)
      .pipe(
        catchError(
          this.handleError<any>('No se pudo actualizar el tablero', [])
        )
      );
  }

  deleteBoard(id_board: number) {
    return this.http.delete<Response>(this.url + 'board/' + id_board);
  }
  //Metodos de miembros
  getBoardMembers(id_board: number): Observable<any> {
    return this.http.get<any>(
      this.url + 'board/members/' + id_board,
      this.httpOptions
    );
  }

  //Metodos de reuniones
  getBoardMettings(id_board: number) {
    return this.http.get<any>(this.url + 'metting_tests/board/' + id_board);
  }
  createBoardMetting(body: MettingTest) {
    return this.http.post<Response>(this.url + 'scope', body);
  }

  //Metodos de Credenciales
  getCredentials(id_board: number) {
    return this.http.get<any>(
      this.url + 'access-credentials/board/' + id_board
    );
  }

  //Metodos de tickets
  getTickets(id_board: number) {
    return this.http.get<any>(this.url + 'ticket/board/' + id_board);
  }
  //Metodos de listas de tablero

  getBoardLists(id_board: number): Observable<BoardList> {
    return this.http.get<BoardList>(this.url + 'board-lists/board/' + id_board);
  }

  addBoardList(board_list: BoardList): Observable<Response> {
    return this.http.post<Response>(this.url + 'board-lists/', board_list);
  }

  deleteBoardList(id_list: number): Observable<Response> {
    return this.http.delete<Response>(this.url + 'board-lists/' + id_list);
  }
  editBoardList(board_list: BoardList, id_list: number): Observable<Response> {
    return this.http.put<Response>(
      this.url + 'board-lists/' + id_list,
      board_list
    );
  }
  //Metodos de SCARDS
  getScard(): Observable<Scard> {
    return this.http.get<Scard>(this.url + 'scrum-card');
  }

  private log(message: string) {
    console.log(`BoardService: ${message}`);
  }

  createObjetive(data: any) {
    return this.http.post<Response>(this.url + 'objetives', data);
  }
  updateObjetive(id_objetive: number, data: any) {
    return this.http.put<Response>(this.url + 'objetives/' + id_objetive, data);
  }
  deleteObjective(id_objetive: number) {
    return this.http.delete<Response>(this.url + 'objetives/' + id_objetive);
  }

  createRequire(data: any) {
    return this.http.post<Response>(this.url + 'requerements', data);
  }
  updateRequire(id_require: number, data: any) {
    return this.http.put<Response>(
      this.url + 'requerements/' + id_require,
      data
    );
  }
  deleteRequire(id_require: number) {
    return this.http.delete<Response>(this.url + 'requerements/' + id_require);
  }

  createScope(data: any) {
    return this.http.post<Response>(this.url + 'scope', data);
  }

  deleteScope(id: number){
    return this.http.delete<Response>(this.url + 'scope/'+ id);
  }
}
