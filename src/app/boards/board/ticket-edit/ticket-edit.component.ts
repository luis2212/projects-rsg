import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tickets } from 'src/app/interfaces/tickets.interface';
import { TicketService } from 'src/app/services/ticket.service';
import Swal from 'sweetalert2';
import { DialogData } from '../../boards.component';

@Component({
  selector: 'app-ticket-edit',
  templateUrl: './ticket-edit.component.html',
  styleUrls: ['./ticket-edit.component.scss'],
})
export class TicketEditComponent implements OnInit {
  editFormTicket: FormGroup;
  id_ticket: number = this.data.id_ticket;
  id_board: number = this.data.id_board;
  currentTicket: Tickets;

  constructor(
    public formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ticketService: TicketService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.getTicketById();
  }

  initForm() {
    this.editFormTicket = this.formBuilder.group({
      ticket_tittle: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      ticket_description: new FormControl('', Validators.required),
      ticket_status: new FormControl('', Validators.required),
      customer_date: new FormControl('', Validators.required),
      resolution_date: new FormControl('', Validators.required),
      ticket_time: new FormControl('', Validators.required),
      solution: new FormControl('', Validators.required),
      archived_ticket: new FormControl('', Validators.required),
      attendees: new FormControl('', Validators.required),
    });
  }

  getTicketById() {
    this.ticketService.getTicketById(this.id_ticket).subscribe((res) => {
      this.currentTicket = res.Ticket;
      this.editFormTicket
        .get('ticket_tittle')
        .setValue(this.currentTicket.ticket_tittle);
      this.editFormTicket
        .get('ticket_description')
        .setValue(this.currentTicket.ticket_description);
      this.editFormTicket.get('color').setValue(this.currentTicket.color);
      this.editFormTicket
        .get('ticket_status')
        .setValue(this.currentTicket.ticket_status);
      this.editFormTicket
        .get('customer_date')
        .setValue(this.currentTicket.customer_date);
      // this.editFormTicket
      //   .get('resolution_date')
      //   .setValue(this.currentTicket.resolution_date);
      // this.editFormTicket
      //   .get('ticket_time')
      //   .setValue(this.currentTicket.ticket_time);
      this.editFormTicket.get('solution').setValue(this.currentTicket.solution);
      this.editFormTicket
        .get('archived_ticket')
        .setValue(this.currentTicket.archived_ticket);
      this.editFormTicket
        .get('attendees')
        .setValue(this.currentTicket.attendees);
    });
  }
  updateStatus(id_ticket: number) {}
  updateTicket() {
    this.ticketService
      .updateTicket(this.id_ticket, this.editFormTicket.value)
      .subscribe((res) => {
        console.log(res);
        if (res.Status != 200) {
          Swal.fire('Ocurrio un error al actualizar', '', 'error');
        } else {
          Swal.fire('Ticket Actualizado Correctamente', '', 'success');
        }
      });
  }
  onDeleteTicket() {
    this.ticketService.deleteTicket(this.id_ticket).subscribe((res) => {
      Swal.fire({
        title: 'Seguro que desea eliminar el ticket?',
        text: 'El ticket se eliminará!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
      }).then((result) => {
        if (result.isConfirmed) {
          if (res.Status != 200) {
            Swal.fire('Ocurrio un error al eliminar', '', 'error');
          } else {
            Swal.fire('Ticket Eliminado Correctamente', '', 'success');
          }
        }
      });
    });
  }
}
