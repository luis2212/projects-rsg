import { Component, Inject, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ScrumServiceService } from 'src/app/services/scrum-service.service';
import { DialogData } from '../../boards.component';
import { Scard } from '../../../interfaces/scard.interface';

import Swal from 'sweetalert2';
import { BoardsService } from '../../boards.service';
import { Checklist } from 'src/app/interfaces/checklist.interface';
import { ChecklistService } from 'src/app/services/checklist.service';
import { Task } from 'src/app/interfaces/task.interface';
import { ChecklistComponent } from './checklist/checklist.component';
import { LabelsService } from 'src/app/services/labels.service';
import { Users } from 'src/app/users/user.interface';

@Component({
  selector: 'app-modal-scrum-update-show',
  templateUrl: './modal-scrum-update-show.component.html',
  styleUrls: ['./modal-scrum-update-show.component.scss'],
})
export class ModalScrumUpdateShowComponent implements OnInit {
  //Formularios
  public registroScrum: FormGroup;
  public registroCheckList: FormGroup;

  public scrumPopulated: Scard;
  //Variables temporales
  public keppers: number[] = [];
  public currentKeppers: Users[] = [];
  public miembros: Users;

  public loading: boolean = true;
  public isShowAddMember: boolean = false;

  public id_currentUser: number;
  public id_list = this.data.id_list;
  public id_board = this.data.id_board;
  public id_scrum: number = this.data.id_scard;
  public id_checklist: number;
  //Variables llenadas API
  public checklists: Checklist[] = [];
  numberTasks: number[] = [];
  public tasks: Task[] = [];
  public users: any;

  public labels: any;
  public currentLabels: any;
  public labelAdd: number[] = [];
  public id_label: string;
  public progressBar: any[] = [];

  public currentLinks: any;
  public linkTitle: string;
  public linkBody: string;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog,
    private scrumService: ScrumServiceService,
    private boardService: BoardsService,
    private checklistService: ChecklistService,
    private labelService: LabelsService
  ) {}

  ngOnInit() {
    this.id_currentUser = JSON.parse(
      localStorage.getItem('currentUser')
    ).user.id_user;
    this.initialForm();
    this.getScards();
  }

  initialComboUsers() {
    //Llenar combo de usuarios
    this.boardService.getBoardMembers(this.id_board).subscribe((members) => {
      this.users = members;

      this.loading = false;
    });
    this.boardService.getBoardById(this.id_board).subscribe((board) => {
      this.labels = board.Board.labels;
    });
  }
  getScards() {
    this.checklists = [];

    this.scrumService
      .getScumById(this.id_scrum)
      .subscribe((scrumData: Scard) => {
        this.scrumPopulated = scrumData;

        scrumData.checklists.forEach((checklist) => {
          let contadorActivo: number = 0;

          checklist.tasks.forEach((task: any) => {
            if (task.task_status) {
              contadorActivo++;
            } else {
              return;
            }
          });
          let newObject = {
            totalTareas: checklist.tasks.length,
            tareasRealizadas: contadorActivo,
          };
          checklist.progress = newObject;
          this.checklists.push(checklist);
        });

        this.currentKeppers = scrumData.keppers;
        this.currentKeppers.forEach((kepper) => {
          this.keppers.push(kepper.id_user);
        });

        scrumData.labels.forEach((label) => {
          this.labelAdd.push(label.id_label);
        });
        this.currentLabels = scrumData.labels;

        this.currentLinks = scrumData.links;
        this.initialToShowOrUpdateForm();
      });
    this.initialComboUsers();
  }
  onDeleteScard() {
    Swal.fire({
      title: '¿Desea eliminar la actividad?',
      text: '¡Se eliminara la actividad definitivamente!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.scrumService.deleteScrum(this.id_scrum).subscribe((res) => {
          if (res.Status == 201) {
            Swal.fire('Eliminada!', 'La actividad fue eliminada.', 'success');
          } else {
            Swal.fire(
              'Ocurrio un error al eliminar!',
              'Intente de nuevo',
              'error'
            );
          }
        });
      }
    });
  }
  initialForm() {
    this.registroScrum = this.formBuilder.group({
      scard_tittle: new FormControl('', Validators.required),
      image_cover: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      scard_description: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      scard_status: new FormControl('', Validators.required),
      start_date_devop: new FormControl('', Validators.required),
      end_date_devop: new FormControl('', Validators.required),
      dedicated_time: new FormControl('', Validators.required),
      archived_scard: new FormControl('', Validators.required),
      id_list: new FormControl('', Validators.required),
      keppers: new FormControl('', Validators.required),
      labels: new FormControl('', Validators.required),
      links: new FormControl('', Validators.required),
    });
    this.registroCheckList = this.formBuilder.group({
      checklist_name: new FormControl('', Validators.required),
      id_scard: new FormControl('', Validators.required),
      user: new FormControl('', Validators.required),
    });
    this.registroCheckList.get('id_scard').setValue(this.id_scrum);
    this.registroCheckList.get('user').setValue(this.id_currentUser);
  }

  initialToShowOrUpdateForm() {
    this.registroScrum
      .get('scard_tittle')
      .setValue(this.scrumPopulated.scard_tittle);
    //this.registroScrum.get('image_cover').setValue(this.scrumPopulated.image_cover);
    this.registroScrum.get('color').setValue(this.scrumPopulated.color);
    this.registroScrum
      .get('scard_description')
      .setValue(this.scrumPopulated.scard_description);
    this.registroScrum
      .get('start_date')
      .setValue(this.scrumPopulated.start_date);
    this.registroScrum.get('end_date').setValue(this.scrumPopulated.end_date);
    this.registroScrum
      .get('scard_status')
      .setValue(this.scrumPopulated.scard_status);
    this.registroScrum
      .get('start_date_devop')
      .setValue(this.scrumPopulated.start_date_devop);
    this.registroScrum
      .get('end_date_devop')
      .setValue(this.scrumPopulated.end_date_devop);
    this.registroScrum.get('keppers').setValue(this.scrumPopulated.keppers);
    this.registroScrum
      .get('dedicated_time')
      .setValue(this.scrumPopulated.dedicated_time);
    this.registroScrum
      .get('scard_status')
      .setValue(this.scrumPopulated.scard_status);
    this.registroScrum.get('id_list').setValue(this.scrumPopulated.id_list);
    this.registroScrum.get('archived_scard').setValue(false);
  }

  async onSubmit() {
    console.log(this.registroScrum.value);
    await this.scrumService
      .updateScrum(this.registroScrum.value, this.id_scrum)
      .subscribe((res) => {
        console.log(res);
        if (!res.Error) {
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Actividad actualizada satisfactoriamente',
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          Swal.fire({
            position: 'top',
            icon: 'error',
            title: 'Algo salio mal, intentelo de nuevo',
            showConfirmButton: false,
            timer: 1500,
          });
        }
      });
  }
  onAddMember() {
    this.isShowAddMember = !this.isShowAddMember;
  }

  onRegisterMember() {
    if (this.keppers.includes(this.miembros.id_user)) {
      Swal.fire('Miembro ya agregado', '', 'info');
    } else {
      this.currentKeppers.push(this.miembros);
      this.keppers.push(this.miembros.id_user);
      console.log(this.keppers);
      this.registroScrum.get('keppers').setValue(this.keppers);
      this.scrumService
        .updateOnlyKeppers(this.keppers, this.id_scrum)
        .subscribe((res) => {
          if (res.Status != 201) {
            Swal.fire('Ocurrio un error al agregar miembros');
          } else {
            Swal.fire('Miembro agregado correctamente', '', 'success');
          }
        });
    }
  }
  onDeleteMember(id_user: number) {
    this.currentKeppers = this.currentKeppers.filter(
      (member) => member.id_user != id_user
    );
    this.keppers = this.keppers.filter((member) => member != id_user);
    this.registroScrum.get('keppers').setValue(this.keppers);
    this.scrumService
      .updateOnlyKeppers(this.keppers, this.id_scrum)
      .subscribe((res) => {
        if (res.Status != 201) {
          Swal.fire('Ocurrio un error al eliminar miembros');
        } else {
          Swal.fire('Miembro eliminado correctamente', '', 'success');
        }
      });
  }

  onRegisterChecklist() {
    this.checklistService
      .createChecklist(this.registroCheckList.value)
      .subscribe((res) => {
        if (res.Error) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se pudo crear la lista de tareas!',
          });
        } else {
          Swal.fire({
            title: 'Lista de tareas creada',
            width: 600,
            padding: '3em',
            color: '#716add',
          });
          this.checklists = [];
          this.registroCheckList.reset();
          this.initialForm();
          this.getScards();
        }
      });
  }

  openDialogChecklist(id_checklist: number) {
    const members = this.users;
    const dialogRef = this.dialog.open(ChecklistComponent, {
      data: { id_checklist, members },
    });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.getScards();
      }, 1500);
    });
  }

  addToScrumLabel() {
    const idCurrent: number[] = [];
    this.currentLabels.forEach((item: any) => {
      idCurrent.push(item.id_label);
    });
    if (
      this.id_label == undefined ||
      this.labelAdd.includes(parseInt(this.id_label)) ||
      idCurrent.includes(parseInt(this.id_label))
    ) {
      Swal.fire('La etiqueta ya fue agregada', '', 'error');
    } else {
      this.labelService
        .getLabelbyId(parseInt(this.id_label))
        .subscribe((res: any) => {
          this.currentLabels.push(res);
        });
      this.labelAdd.push(parseInt(this.id_label));

      this.registroScrum.get('labels').setValue(this.labelAdd);
      this.scrumService
        .updateOnlyLabels(this.labelAdd, this.id_scrum)
        .subscribe((res) => {
          if (res.Status != 201) {
            Swal.fire('Ocurrio un error', '', 'warning');
          } else {
            Swal.fire('Etiqueta agregada correctamente', '', 'success');
          }
        });
    }
  }
  onDeleteLabelToScard(id_label: number) {
    this.currentLabels = this.currentLabels.filter(function (label: any) {
      return label.id_label != id_label;
    });
    this.labelAdd = this.labelAdd.filter((id) => {
      return id != id_label;
    });
    console.log(this.labelAdd);
    this.registroScrum.get('labels').setValue(this.labelAdd);
    this.scrumService
      .updateOnlyLabels(this.labelAdd, this.id_scrum)
      .subscribe((res) => {
        if (res.Status != 201) {
          Swal.fire('Ocurrio un error', '', 'warning');
        } else {
          Swal.fire('Etiqueta eliminada correctamente', '', 'success');
        }
      });
  }
  onAddLink() {
    const newLink = [
      {
        link_content: this.linkBody,
        link_name: this.linkTitle,
        id_scard: this.id_scrum,
      },
    ];
    this.scrumService.createLinkScard(newLink).subscribe((res) => {
      if (res.Status != 201) {
        Swal.fire('Ocurrio un error al crear enlace', '', 'error');
      } else {
        Swal.fire('Enlace creado correctamente', '', 'success');
        this.getScards();
        this.linkBody = '';
        this.linkTitle = '';
      }
    });
  }
  onDeleteLink(id_link: number) {
    this.scrumService.deleteLinkScard(id_link).subscribe((res) => {
      if (res.Status != 201) {
        Swal.fire('Ocurrio un error al eliminar', '', 'error');
      } else {
        Swal.fire('Enlace eliminado correctamente', '', 'success');
        this.getScards();
      }
    });
  }
}
