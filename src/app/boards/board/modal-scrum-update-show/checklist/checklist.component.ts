import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/boards/boards.component';
import { Checklist } from 'src/app/interfaces/checklist.interface';
import { Task } from 'src/app/interfaces/task.interface';
import { ChecklistService } from 'src/app/services/checklist.service';
import { Users } from 'src/app/users/user.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss'],
})
export class ChecklistComponent implements OnInit {
  public id_checklist: number = this.data.id_checklist;
  public members: Users[] = this.data.members;
  public checklist: Checklist;
  public tasks: Task[];

  public statusTask: boolean;

  public addTaskForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private checklistService: ChecklistService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.resData();
    this.initForms();
    this.initValuesForm();
  }

  initForms(): void {
    this.addTaskForm = this.formBuilder.group({
      task_description: new FormControl('', Validators.required),
      task_status: new FormControl('', Validators.required),
      scheduled_data: new FormControl('', Validators.required),
      start_date: new FormControl(''),
      end_date: new FormControl(''),
      task_time: new FormControl(''),
      task_filename: new FormControl(''),
      task_link: new FormControl(''),
      user_attend: new FormControl('', Validators.required),
      id_checklist: new FormControl('', Validators.required),
    });
  }

  initValuesForm() {
    this.addTaskForm.get('id_checklist').setValue(this.id_checklist);
    this.addTaskForm.get('task_status').setValue(false);
  }

  onCreateTask() {
    this.checklistService
      .createTask(this.addTaskForm.value)
      .subscribe((res) => {
        if (res.Status != 201) {
          Swal.fire('Ocurrio un error al crear tarea', '', 'error');
        } else {
          Swal.fire('Tarea creada correctamente', '', 'success');
          this.addTaskForm.reset();
          this.initValuesForm();
          this.resData();
        }
      });
  }
  onDeleteTask(id_task: number) {
    Swal.fire({
      title: 'Seguro que desea eliminar la tarea?',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.checklistService.deleteTask(id_task).subscribe((res) => {
          if (res.Status !== 201) {
            Swal.fire('Error al eliminar tarea', '', 'error');
          } else {
            Swal.fire('Tarea Eliminada Correctamente', '', 'success');
            this.resData();
          }
        });
      }
    });
  }
  onUpdateTask(event: any, id_task: number, task: Task) {
    this.addTaskForm.get('id_checklist').setValue(this.id_checklist);
    //this.addTaskForm.get('task_filename').setValue(task.task_filename);
    this.addTaskForm.get('task_link').setValue(task.task_link);
    this.addTaskForm.get('task_time').setValue(task.task_time);
    this.addTaskForm.get('end_date').setValue(task.end_date);
    this.addTaskForm.get('start_date').setValue(task.start_date);
    this.addTaskForm.get('scheduled_data').setValue(task.scheduled_data);
    this.addTaskForm.get('task_description').setValue(task.task_description);
    this.addTaskForm.get('task_status').setValue(event.target.checked);
    console.log(this.addTaskForm.value);
    this.checklistService
      .updateTask(id_task, this.addTaskForm.value)
      .subscribe((res) => {
        console.log(res);
        if (res.Status != 201) {
          Swal.fire('Ocurrio un error', '', 'warning');
        } else {
          Swal.fire('Tarea Realizada', '', 'success');
          this.resData();
        }
      });
  }

  onDeleteChecklist() {
    Swal.fire({
      title: 'Estas seguro?',
      text: 'Se eliminarán todas las tareas!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.checklistService
          .deleteChecklist(this.id_checklist)
          .subscribe((res) => {
            if (res.Status !== 201) {
              Swal.fire('Ocurrio un error al eliminar', '', 'error');
            } else {
              Swal.fire('Lista eliminada exitosamente', '', 'success');
            }
          });
      }
    });
  }
  resData() {
    this.checklistService
      .getChecklistById(this.id_checklist)
      .subscribe((res) => {
        this.checklist = res;
        this.tasks = res.tasks;
      });
  }
}
