import {
  Component,
  ElementRef,
  HostListener,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ScrumServiceService } from 'src/app/services/scrum-service.service';
import { Users } from 'src/app/users/user.interface';
import { UsersService } from 'src/app/users/users.service';
import Swal from 'sweetalert2';
import { DialogData } from '../../boards.component';
import { BoardsService } from '../../boards.service';

@Component({
  selector: 'app-modal-scrum',
  templateUrl: './modal-scrum.component.html',
  styleUrls: ['./modal-scrum.component.scss'],
})
export class ModalScrumComponent implements OnInit {
  public archivo = {
    nombreArchivo: 1,
    base64textString: '',
  };
  public selectedFile: any;
  public registroScrum: FormGroup;
  public registroMiembro: FormGroup;
  public users: Users[];
  public keppers: number[] = [];
  public keppersName: string[] = [];
  public loading: boolean = true;
  public isAdd: boolean = false;

  public id_list = this.data.id_list;
  public id_board = this.data.id_board;
  constructor(
    private formBuilder: FormBuilder,
    private boardService: BoardsService,
    private scrumService: ScrumServiceService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.initialComboUsers();
    this.initialForm();
    this.initialToShowOrUpdateForm();
  }

  initialComboUsers() {
    //Llenar combo de usuarios
    this.boardService.getBoardMembers(this.id_board).subscribe((members) => {
      this.users = members;
      this.loading = false;
    });
  }

  initialForm() {
    this.registroMiembro = this.formBuilder.group({
      miembros: new FormControl({}, Validators.required),
    });
    this.registroScrum = this.formBuilder.group({
      scard_tittle: new FormControl('', Validators.required),
      image_cover: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      scard_description: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      scard_status: new FormControl('', Validators.required),
      start_date_devop: new FormControl('', Validators.required),
      end_date_devop: new FormControl('', Validators.required),
      dedicated_time: new FormControl('', Validators.required),
      archived_scard: new FormControl('', Validators.required),
      id_list: new FormControl('', Validators.required),
      keppers: new FormControl('', Validators.required),
    });
  }

  initialToShowOrUpdateForm() {
    this.registroScrum.get('id_list').setValue(this.id_list);
    this.registroScrum.get('keppers').setValue(this.keppers);
    this.registroScrum.get('archived_scard').setValue(false);
  }

  onSubmit() {
    this.scrumService.saveScrum(this.registroScrum.value).subscribe((res) => {
      if (!res.Error) {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Actividad creada satisfactoriamente',
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Algo salio mal, intentelo de nuevo',
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  }

  onRegisterMember() {
    let id_Member = parseInt(this.registroMiembro.value.miembros);

    let nameMember = this.registroMiembro.value.miembros;

    if (this.keppers.includes(id_Member) === false) {
      this.keppers.push(id_Member);
      this.keppersName.push(nameMember);
      this.toastr.info('Miembro agregado correctamente');
    } else {
      this.toastr.warning('Miembro ya esta agregado');
    }
  }

  onAddMember() {
    this.isAdd = !this.isAdd;
  }
}
