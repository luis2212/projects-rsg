import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Credentials } from 'src/app/interfaces/credential.interface';
import { CredentialService } from 'src/app/services/credential.service';
import Swal from 'sweetalert2';
import { DialogData } from '../../boards.component';

@Component({
  selector: 'app-credential-edit',
  templateUrl: './credential-edit.component.html',
  styleUrls: ['./credential-edit.component.scss'],
})
export class CredentialEditComponent implements OnInit {
  public editCredentialForm: FormGroup;
  public id_credential: number = this.data.id_credential;
  public id_board: number = this.data.id_board;
  public currentCredential: Credentials;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private credentialService: CredentialService
  ) {}

  ngOnInit(): void {
    this.initForms();
    this.getCredential();
  }
  initForms() {
    this.editCredentialForm = this.formBuilder.group({
      credential_description: new FormControl('', Validators.required),
      credential_links: new FormControl('', Validators.required),
      credential_filename: new FormControl('', Validators.required),
      credential_path: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
    });
  }
  getCredential() {
    this.credentialService
      .getOneCredential(this.id_credential)
      .subscribe((res) => {
        this.currentCredential = res;
        this.editCredentialForm
          .get('credential_description')
          .setValue(this.currentCredential.credential_description);
        this.editCredentialForm
          .get('credential_links')
          .setValue(this.currentCredential.credential_links);

        this.editCredentialForm
          .get('credential_path')
          .setValue(this.currentCredential.credential_path);
        this.editCredentialForm
          .get('id_board')
          .setValue(this.currentCredential.id_board);
      });
  }
  onEditCredential() {
    this.credentialService
      .updateCredential(this.id_credential, this.editCredentialForm.value)
      .subscribe((res) => {
        if (res.Status != 201) {
          Swal.fire('Ocurrio un error al editar', '', 'error');
        } else {
          Swal.fire('Credencial Actualizada', '', 'success');
        }
      });
  }
  onDeleteCredential() {
    Swal.fire({
      title: 'Seguro que deseas eliminar la credencial?',
      text: 'Se eliminarán todos los datos!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.credentialService
          .deleteCredental(this.id_credential)
          .subscribe((res) => {
            if (res.Status !== 201) {
              Swal.fire('Ocurrio un error al eliminar', '', 'error');
            } else {
              Swal.fire('Credencial Eliminada', '', 'success');
            }
          });
      }
    });
  }
}
