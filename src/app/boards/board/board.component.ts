import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { Console } from 'console';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Board } from '../board.interface';
import { BoardsService } from '../boards.service';
import { ModalScrumUpdateShowComponent } from './modal-scrum-update-show/modal-scrum-update-show.component';
import { ModalScrumComponent } from './modal-scrum/modal-scrum.component';
import { CredentialEditComponent } from './credential-edit/credential-edit.component';
import { MettingEditComponent } from './metting-edit/metting-edit.component';
import { TicketEditComponent } from './ticket-edit/ticket-edit.component';
import { LabelsService } from '../../services/labels.service';
import { MettingTest } from '../../interfaces/mettingtest.interface';
import { Credentials } from 'src/app/interfaces/credential.interface';
import { Tickets } from 'src/app/interfaces/tickets.interface';
import { Users } from 'src/app/users/user.interface';
import { UsersService } from 'src/app/users/users.service';
import { MettingService } from 'src/app/services/metting.service';
import { TicketService } from 'src/app/services/ticket.service';
import { CredentialService } from 'src/app/services/credential.service';
import { ColorEvent } from 'ngx-color';
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  //variables de ID
  private idBoardF: number = parseInt(this.rutaActiva.snapshot.params.id);
  public id_list: number;
  public boardFounded: Board;
  //Variables de datos API
  public members: any;
  public moderator: any;
  public lists: any;
  public scards: any;
  public metting_test: MettingTest[] = [];
  public credentials: Credentials[] = [];
  public tickets: Tickets[] = [];
  public attendees: number[] = [];
  public metting_members: any[] = [];
  //Variables de form
  public addListForm: FormGroup;
  public addLabelForm: FormGroup;
  public addMettingForm: FormGroup;
  public attendeesForm: FormGroup;
  public addTicketForm: FormGroup;
  public addCredentialForm: FormGroup;
  public addCommentForm: FormGroup;
  //Variables de visualización
  public isAddList: boolean = false;
  public isEditList: boolean = false;
  public currentDate: Date = new Date();
  public currentUser: number;
  public newObjetivo = '';
  public newRequire = '';
  public newScope = '';

  public colorpicker =
    '<color-twitter formControlName="color"></color-twitter> <h1>hi</h1>';

  constructor(
    private rutaActiva: ActivatedRoute,
    private boardService: BoardsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private ngxLoader: NgxUiLoaderService,
    private labelService: LabelsService,
    private userService: UsersService,
    private mettingService: MettingService,
    private ticketService: TicketService,
    private credentialService: CredentialService
  ) {
    const UserActual = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser = UserActual.user.id_user;
  }

  ngOnInit() {
    this.ngxLoader.start();
    this.restData();
    this.initForms();
  }

  restData() {
    this.boardService.getBoardById(this.idBoardF).subscribe((board) => {
      this.boardFounded = board.Board;
      this.ngxLoader.stop();
    });
    this.boardService.getBoardMembers(this.idBoardF).subscribe((members) => {
      this.members = members;
    });
    this.boardService.getBoardLists(this.idBoardF).subscribe((list: any) => {
      this.lists = list.Lists;
    });
    this.boardService
      .getBoardMettings(this.idBoardF)
      .subscribe((metting: any) => {
        this.metting_test = metting.Metting_Test;
      });
    this.boardService
      .getCredentials(this.idBoardF)
      .subscribe((credentials: any) => {
        this.credentials = credentials.Credentials;
      });
    this.boardService.getTickets(this.idBoardF).subscribe((tickets: any) => {
      this.tickets = tickets.AllTickets;
    });

    this.metting_members = [];
    this.attendees.forEach((attendee) => {
      this.userService.getUserById(attendee).subscribe((res) => {
        this.metting_members.push(res.User);
      });
    });
  }

  initForms() {
    this.addListForm = this.formBuilder.group({
      list_tittle: new FormControl('', Validators.required),
      archived_list: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
    });
    this.addLabelForm = this.formBuilder.group({
      color: new FormControl('', Validators.required),
      label_tittle: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
    });
    this.addMettingForm = this.formBuilder.group({
      title_metting: new FormControl('', Validators.required),
      description_metting: new FormControl('', Validators.required),
      scheduled_date: new FormControl('', Validators.required),
      start_time: new FormControl('', Validators.required),
      end_time: new FormControl('', Validators.required),
      platform: new FormControl('', Validators.required),
      memorandum: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
      attendees: new FormControl('', Validators.required),
    });

    this.addTicketForm = this.formBuilder.group({
      ticket_tittle: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      ticket_description: new FormControl('', Validators.required),
      ticket_status: new FormControl('', Validators.required),
      customer_date: new FormControl('', Validators.required),
      resolution_date: new FormControl(''),
      ticket_time: new FormControl('', Validators.required),
      archived_ticket: new FormControl('', Validators.required),
      solution: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
      attendees: new FormControl('', Validators.required),
    });

    this.attendeesForm = this.formBuilder.group({
      attendees: new FormControl('', Validators.required),
    });

    this.addCredentialForm = this.formBuilder.group({
      credential_description: new FormControl('', Validators.required),
      credential_links: new FormControl('', Validators.required),
      credential_filename: new FormControl('', Validators.required),
      credential_path: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
    });
    this.addCommentForm = this.formBuilder.group({
      comment_content: new FormControl('', Validators.required),
      id_credential: new FormControl('', Validators.required),
      users: new FormControl('', Validators.required),
    });

    this.initDefValuesForm();
  }
  initDefValuesForm() {
    this.addLabelForm.get('id_board').setValue(this.idBoardF);

    this.addListForm.get('archived_list').setValue(false);
    this.addListForm.get('id_board').setValue(this.idBoardF);

    this.addMettingForm.get('id_board').setValue(this.idBoardF);
    this.addMettingForm.get('attendees').setValue(this.attendees);

    this.addTicketForm.get('id_board').setValue(this.idBoardF);
    this.addTicketForm.get('attendees').setValue(this.attendees);
    this.addTicketForm.get('archived_ticket').setValue(false);
    this.addTicketForm.get('ticket_status').setValue(false);
    this.addTicketForm.get('customer_date').setValue(this.currentDate);
    this.addTicketForm.get('resolution_date').setValue(this.currentDate);
    this.addTicketForm.get('solution').setValue('Solución Pendiente');
    this.addTicketForm.get('ticket_time').setValue('00:00:00');

    this.addCredentialForm.get('id_board').setValue(this.idBoardF);

    this.addCommentForm.get('id_credential');
    this.addCommentForm.get('users').setValue(this.currentUser);
  }
  openDialogScrum(id_list: number) {
    let id_board = this.idBoardF;
    const dialogRef = this.dialog.open(ModalScrumComponent, {
      data: { id_list, id_board },
    });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.restData();
      }, 2000);
    });
  }

  openDialogEditScrum(id_scard: number, id_list: number) {
    let id_board = this.idBoardF;
    const dialogRef = this.dialog.open(ModalScrumUpdateShowComponent, {
      data: { id_scard, id_board, id_list },
    });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.restData();
      }, 5000);
    });
  }

  onChangeStatus() {
    //Agregar función de cambio de estado
  }

  onAddList() {
    console.log(this.addListForm.value);
    if (this.isAddList) {
      this.boardService
        .addBoardList(this.addListForm.value)
        .subscribe((result) => {
          console.log(result);
          if (result.Status == 201) {
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Lista creada',
              showConfirmButton: false,
              timer: 1000,
            });
            this.restData();
            this.isAddList = false;

            this.initForms();
          } else {
            Swal.fire({
              position: 'top',
              icon: 'error',
              title: 'Ocurrio un error al crear la lista',
              showConfirmButton: false,
              timer: 1500,
            });
          }
        });
    } else {
      this.boardService
        .editBoardList(this.addListForm.value, this.id_list)
        .subscribe((result) => {
          if (result.Status == 201) {
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Lista Actualizada',
              showConfirmButton: false,
              timer: 1000,
            });
            this.restData();
            this.isEditList = false;
            this.addListForm.reset();
            this.initForms();
          } else {
            Swal.fire({
              position: 'top',
              icon: 'error',
              title: 'Ocurrio un error al crear la lista',
              showConfirmButton: false,
              timer: 1500,
            });
          }
        });
    }
  }
  onEditList(id_list: number, list_tittle: string) {
    this.addListForm.get('list_tittle').setValue(list_tittle);
    this.id_list = id_list;
    this.isEditList = !this.isEditList;
  }
  onDeleteList(id_list: number) {
    Swal.fire({
      title: '¿Desea eliminar la lista?',
      text: 'Se eliminaran todas las tarjetas asignadas a la lista!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.boardService.deleteBoardList(id_list).subscribe((res) => {
          if (res.Status == 201) {
            Swal.fire('Eliminada!', 'La lista fue eliminada.', 'success');
            this.restData();
          } else {
            Swal.fire(
              'Ocurrio un error al eliminar!',
              'Intente de nuevo',
              'error'
            );
          }
        });
      }
    });
  }
  showAddList() {
    this.isAddList = !this.isAddList;
    this.addListForm.reset();
    this.initForms();
  }

  addLabel() {
    this.labelService.createLabels(this.addLabelForm.value).subscribe((res) => {
      if (res.Status !== 201) {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Ocurrio un error al crear la etiqueta',
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Etiqueta creada',
          showConfirmButton: false,
          timer: 500,
        });
        this.restData();
        this.addCommentForm.reset();
        this.initForms();
      }
    });
  }
  onDeleteLabel(id_label: number) {
    Swal.fire({
      title: '¿Desea eliminar la etiqueta?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.labelService.deleteLabels(id_label).subscribe((res) => {
          if (res.Status == 201) {
            Swal.fire('Etiqueta eliminada!', '', 'success');
            this.restData();
          } else {
            Swal.fire({
              title: 'Ocurrio un error al eliminar!',
              footer: res.Message,
              icon: 'error',
            });
          }
        });
      }
    });
  }

  onAddMetting() {
    this.mettingService
      .createMettingTest(this.addMettingForm.value)
      .subscribe((result) => {
        if (result.Status != 201) {
          Swal.fire('Ocurrio un error al crear la reunión', '', 'error');
        } else {
          Swal.fire('Reunión creada exitosamente', '', 'success');
          this.restData();
        }
      });
  }

  addMemberToMetting() {
    let id_attendees = parseInt(this.attendeesForm.value.attendees);
    if (this.attendees.includes(id_attendees) === false) {
      this.attendees.push(id_attendees);
      Swal.fire('Usuario agregado', '', 'success');
      this.addMettingForm.get('attendees').setValue(this.attendees);
      this.restData();
    } else {
      Swal.fire('Usuario ya en reunión', '', 'info');
    }
  }
  onDeleteMemberToMetting(id_user: number) {
    Swal.fire('Integrante eliminado', '', 'success');
    this.attendees = this.attendees.filter((id) => {
      return id != id_user;
    });
    this.restData();
    this.addMettingForm.get('attendees').setValue(this.attendees);
  }

  onEditMetting(id_metting: number) {
    const id_board = this.idBoardF;
    const dialogRef = this.dialog.open(MettingEditComponent, {
      data: { id_metting, id_board },
    });
    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.restData();
      }, 1500);
    });
  }

  onAddTicket() {
    this.ticketService
      .createTicket(this.addTicketForm.value)
      .subscribe((res) => {
        if (res.Error) {
          Swal.fire('Ocurrio un eror al crear un ticket', '', 'warning');
        } else {
          Swal.fire('Ticket creado correctamente', '', 'success');
          this.restData();
        }
      });
  }

  onEditTicket(id_ticket: number) {
    const id_board = this.idBoardF;
    const dialogRef = this.dialog.open(TicketEditComponent, {
      data: { id_ticket, id_board },
    });
    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.restData();
      }, 1500);
    });
  }
  onCreateCredential() {
    this.credentialService
      .createCredentials(this.addCredentialForm.value)
      .subscribe((res) => {
        if (res.Error) {
          Swal.fire('Error al crear credencial', '', 'warning');
        } else {
          Swal.fire('Credencial creada correctamente', '', 'success');
          this.restData();
        }
      });
  }
  onEditCredential(id_credential: number) {
    const id_board = this.idBoardF;
    const dialogRef = this.dialog.open(CredentialEditComponent, {
      data: { id_credential, id_board },
    });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.restData();
      }, 1500);
    });
  }

  onCreateObjetive() {
    if (this.newObjetivo.length > 0) {
      const objCreated = {
        objetive_tittle: this.newObjetivo,
        target_status: false,
        id_board: this.idBoardF,
      };
      this.boardService.createObjetive(objCreated).subscribe((result) => {
        if (result.Status != 201) {
          Swal.fire('Error al crear objetivo', '', 'warning');
        } else {
          Swal.fire('Objetivo Agregado', '', 'success');
          this.restData();
          this.newObjetivo = '';
        }
      });
    }
  }

  onObjetiveStatusChange(event: any, id_objetive: number) {
    const newStatus = {
      target_status: event.target.checked,
    };
    this.boardService
      .updateObjetive(id_objetive, newStatus)
      .subscribe((res) => {
        this.restData();
      });
  }

  onDeleteObjetive(id_objetive: number) {
    this.boardService.deleteObjective(id_objetive).subscribe((res) => {
      this.restData();
    });
  }

  onCreateRequire() {
    if (this.newRequire.length > 0) {
      const reqCreated = {
        request_description: this.newRequire,
        request_status: false,
        id_board: this.idBoardF,
      };
      this.boardService.createRequire(reqCreated).subscribe((result) => {
        console.log(result);
        if (result.Status != 201) {
          Swal.fire('Error al crear requerimiento', '', 'warning');
        } else {
          Swal.fire('Requerimiento Agregado', '', 'success');
          this.restData();
          this.newRequire = '';
        }
      });
    }
  }

  onDeleteRequire(id_request: number) {
    this.boardService.deleteRequire(id_request).subscribe((result) => {
      this.restData();
    });
  }

  onUpdateRequire(event: any, id_request: number) {
    const newStatus = {
      request_status: event.target.checked,
    };
    this.boardService.updateRequire(id_request, newStatus).subscribe((res) => {
      this.restData();
    });
  }

  onCreateScope() {
    const newScope = {
      scope_description: this.newScope,
      id_board: this.idBoardF,
    };
    this.boardService.createScope(newScope).subscribe((res) => {
      if (res.Status !== 201) {
        Swal.fire('Error al crear alcance', '', 'warning');
      } else {
        Swal.fire('Alcance agregado correctamente', '', 'success');
        this.restData();
        this.newScope = '';
      }
    });
  }
  onDeleteScope(id_scope: number) {
    this.boardService.deleteScope(id_scope).subscribe((res) => {
      if (res.Status !== 201) {
        Swal.fire('Error eliminar', '', 'warning');
      } else {
        Swal.fire('Alcance Eliminado correctamente', '', 'success');
        this.restData();
      }
    });
  }
}
