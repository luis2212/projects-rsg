import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MettingEditComponent } from './metting-edit.component';

describe('MettingEditComponent', () => {
  let component: MettingEditComponent;
  let fixture: ComponentFixture<MettingEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MettingEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MettingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
