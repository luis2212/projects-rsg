import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MettingTest } from 'src/app/interfaces/mettingtest.interface';
import { MettingService } from 'src/app/services/metting.service';
import { UsersService } from 'src/app/users/users.service';
import Swal from 'sweetalert2';
import { BoardsService } from '../../boards.service';

@Component({
  selector: 'app-metting-edit',
  templateUrl: './metting-edit.component.html',
  styleUrls: ['./metting-edit.component.scss'],
})
export class MettingEditComponent implements OnInit {
  public editMettingForm: FormGroup;
  public id_metting: number = this.data.id_metting;
  public id_board: number = this.data.id_board;
  public currentMetting: MettingTest;
  attendeesForm: FormGroup;
  public attendees: any[] = [];
  public members: any = [];
  public metting_members: any = [];

  constructor(
    public formBuilder: FormBuilder,
    public mettingService: MettingService,
    public usersService: UsersService,
    public boardService: BoardsService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.initForms();
    this.getMetting();
  }

  initForms() {
    this.editMettingForm = this.formBuilder.group({
      title_metting: new FormControl('', Validators.required),
      description_metting: new FormControl('', Validators.required),
      scheduled_date: new FormControl('', Validators.required),
      start_time: new FormControl('', Validators.required),
      end_time: new FormControl('', Validators.required),
      platform: new FormControl('', Validators.required),
      memorandum: new FormControl('', Validators.required),
      id_board: new FormControl('', Validators.required),
      attendees: new FormControl('', Validators.required),
    });
    this.attendeesForm = this.formBuilder.group({
      attendees: new FormControl('', Validators.required),
    });
  }
  getMetting() {
    this.mettingService.getMettingById(this.id_metting).subscribe((res) => {
      this.currentMetting = res.Metting_test;
      this.metting_members = this.currentMetting.attendees;
      this.editMettingForm
        .get('title_metting')
        .setValue(this.currentMetting.title_metting);
      this.editMettingForm
        .get('description_metting')
        .setValue(this.currentMetting.description_metting);
      this.editMettingForm
        .get('scheduled_date')
        .setValue(this.currentMetting.scheduled_date);
      this.editMettingForm
        .get('start_time')
        .setValue(this.currentMetting.start_time);
      this.editMettingForm
        .get('end_time')
        .setValue(this.currentMetting.end_time);
      this.editMettingForm
        .get('platform')
        .setValue(this.currentMetting.platform);
      this.editMettingForm
        .get('memorandum')
        .setValue(this.currentMetting.memorandum);
      this.editMettingForm.get('id_board').setValue(this.id_board);
      this.editMettingForm
        .get('attendees')
        .setValue(this.currentMetting.attendees);

      this.metting_members.forEach((member: any) => {
        this.attendees.push(member.id_user);
      });
    });

    this.attendees.forEach((attendee: any) => {
      this.usersService.getUserById(attendee).subscribe((res) => {
        this.metting_members.push(res.User);
      });
    });
    this.boardService.getBoardMembers(this.id_board).subscribe((members) => {
      this.members = members;
    });
  }
  onEditMetting() {
    this.mettingService
      .updateMetting(this.id_metting, this.editMettingForm.value)
      .subscribe((res) => {
        if (res.Status !== 201) {
          Swal.fire('Ocurrio un error al actualizar reunión', '', 'error');
        } else {
          Swal.fire('Reunión Actualizada Correctamente', '', 'success');
        }
      });
  }
  addMemberToMetting() {
    let id_attendees = parseInt(this.attendeesForm.value.attendees);
    if (this.attendees.includes(id_attendees) === false) {
      this.attendees.push(id_attendees);

      this.mettingService
        .updateMember(this.id_metting, this.attendees)
        .subscribe((res) => {
          if (res.Status != 201) {
            Swal.fire('Ocurrio un error al agregar', '', 'error');
          } else {
            Swal.fire('Usuario agregado', '', 'success');
            this.members = [];
            this.attendees = [];
            this.getMetting();
          }
        });
    } else {
      Swal.fire('Usuario ya en reunión', '', 'info');
    }
  }
  onDeleteMemberToMetting(id_user: number) {
    this.attendees = this.attendees.filter((id) => {
      return id != id_user;
    });
    console.log(this.attendees);

    this.mettingService
      .updateMember(this.id_metting, this.attendees)
      .subscribe((res) => {
        if (res.Status != 201) {
          Swal.fire('Error al eliminar', '', 'success');
        } else {
          Swal.fire('Integrante eliminado', '', 'success');
          this.members = [];
          this.attendees = [];
          this.getMetting();
        }
      });
  }
  onDeleteMetting() {
    Swal.fire({
      title: 'Seguro que deseas eliminar la reunión?',
      text: 'Se eliminará la reunión!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.mettingService.deleteMetting(this.id_metting).subscribe((res) => {
          if (res.Status !== 201) {
            Swal.fire('Ocurrio un error al eliminar', '', 'error');
          } else {
            Swal.fire('Reunión Eliminada');
          }
        });
      }
    });
  }
}
