import { DataGroup } from '../group/group.interface';
import { Credentials } from '../interfaces/credential.interface';
import { Label } from '../interfaces/labels.interface';
import { MettingTest } from '../interfaces/mettingtest.interface';
import { Objetive } from '../interfaces/objetive.interface';
import { Requerements } from '../interfaces/requeriments.interface';
import { Scope } from '../interfaces/scope.interface';
import { Tickets } from '../interfaces/tickets.interface';
import { Users } from '../users/user.interface';

export interface Board {
  id_board: number;
  board_name: string;
  description_board: string;
  start_date: Date;
  end_date: Date;
  project_manager: number;
  team: number;
  users: Users;
  board_finish: boolean;
  archived_board: boolean;
  objetive: Objetive[];
  requerements: Requerements[];
  labels: Label[];
  scope: Scope[];
  credentials: Credentials[];
  group: DataGroup;
  metting_test: MettingTest[];
  tickets: Tickets[];
  isFavorite?: boolean;
}
