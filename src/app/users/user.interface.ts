export interface Users {
  id_user: number;
  username: string;
  name: string;
  lastname: string;
  password: string;
  photo: string;
  role_id: number;
  role: any;
  email: string;
}
