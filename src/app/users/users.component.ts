import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { ModalCreateUserComponent } from './modal-create-user/modal-create-user.component';
import { ModalEditUserComponent } from './modal-edit-user/modal-edit-user.component';
import { Users } from './user.interface';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  public users: any;
  constructor(
    private usersService: UsersService,
    public dialog: MatDialog,
    public userService: UsersService,
    private ngxLoader: NgxUiLoaderService
  ) {}

  ngOnInit() {
    this.getUsers();
    this.ngxLoader.start();
  }

  getUsers() {
    return this.usersService.getUsers().subscribe((res) => {
      this.users = res;
      this.ngxLoader.stop();
    });
  }
  openCreateUser() {
    const dialogRef = this.dialog.open(ModalCreateUserComponent);

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.getUsers();
      }, 1000);
    });
  }
  onDeleteUser(id_user: number) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Estas seguro?',
        text: 'Ya no podrás deshacer esta acción!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No',
        reverseButtons: true,
      })
      .then((result: any) => {
        if (result.isConfirmed) {
          this.userService.deleteUser(id_user).subscribe((res) => {
            if (!res.Error) {
              swalWithBootstrapButtons.fire(
                'Eliminado!',
                'Usuario fue eliminado correctamente.',
                'success'
              );
              this.getUsers();
            } else {
              swalWithBootstrapButtons.fire(
                'Algo Salio mal!',
                'Vuelva a intentarlo.',
                'error'
              );
              console.log(res.Error);
              this.getUsers();
            }
          });
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'Ningun usuario fue eliminado :)',
            'error'
          );
        }
      });
  }

  onEditUser(user: Users) {
    const dialogRef = this.dialog.open(ModalEditUserComponent, { data: user });

    dialogRef.afterClosed().subscribe((result) => {
      setTimeout(() => {
        this.getUsers();
      }, 1000);
    });
  }
}
