import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/boards/boards.component';
import { RoleService } from 'src/app/role/role.service';
import Swal from 'sweetalert2';
import { Users } from '../user.interface';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-modal-edit-user',
  templateUrl: './modal-edit-user.component.html',
  styleUrls: ['./modal-edit-user.component.scss']
})
export class ModalEditUserComponent implements OnInit {

  public updateUser: FormGroup;
  public img_def = "img/defa.jpg";
  public roles: any;
  public id_user: number = this.data.id_user;


  constructor(
    public formBuilder: FormBuilder,
    public userService: UsersService,
    public roleService: RoleService,
    @Inject(MAT_DIALOG_DATA) public data: Users,
  ) { }

  ngOnInit() {
    this.initialFormUpdateUser();
    this.fetchData();
  }

  initialFormUpdateUser() {
    this.updateUser = this.formBuilder.group({
      username: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      photo: new FormControl(''),
      email: new FormControl('', Validators.required),
      role_id: new FormControl('', Validators.required),
    })

    this.updateUser.get('username').setValue(this.data.username);
    this.updateUser.get('name').setValue(this.data.name);
    this.updateUser.get('lastname').setValue(this.data.lastname);
    this.updateUser.get('role_id').setValue(this.data.role.role_id);
    this.updateUser.get('email').setValue(this.data.email);
    //this.updateUser.get('password').setValue(this.data.password);
  }

  fetchData() {
    this.roleService.getRoles().subscribe(res => {
      this.roles = (res);
    })
  }

  onUpdateUser() {
    this.userService.updateUser(this.updateUser.value, this.id_user).subscribe(res => {
      console.log(res);
      if (!res.Error) {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Usuario Actualizado :)',
          showConfirmButton: false,
          timer: 1500
        })

      } else {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Error al Actualizar Usuario :(',
          showConfirmButton: false,
          timer: 1500
        })
      }

    });
  }
}
