import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Response } from '../interfaces/response.interface';
import { Users } from './user.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  url: string = environment.baseUrl + 'users/';
  constructor(private http: HttpClient) {}

  private token: string;
  private user: any;

  getUsers(): Observable<Users> {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.token = this.user.token;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.token,
      }),
    };
    return this.http.get<Users>(this.url, httpOptions);
  }

  getUserById(id_user: number) {
    return this.http.get<any>(this.url + id_user);
  }

  createUser(body: Users) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.token = this.user.token;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.token,
      }),
    };
    return this.http.post<Response>(this.url, body, httpOptions);
  }

  deleteUser(id_user: number) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.token = this.user.token;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.token,
      }),
    };
    return this.http.delete<Response>(this.url + id_user, httpOptions);
  }

  updateUser(body: Users, id_user: number) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.token = this.user.token;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.token,
      }),
    };
    return this.http.put<Response>(this.url + id_user, body, httpOptions);
  }
}
