import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Role } from 'src/app/interfaces/role.interface';
import { RoleService } from 'src/app/role/role.service';
import { UsersService } from '../users.service';
import { Response } from 'src/app/interfaces/response.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-create-user',
  templateUrl: './modal-create-user.component.html',
  styleUrls: ['./modal-create-user.component.scss'],
})
export class ModalCreateUserComponent implements OnInit {
  public registerUser: FormGroup;
  public img_def = 'img/defa.jpg';
  public roles: any;

  constructor(
    public formBuilder: FormBuilder,
    public userService: UsersService,
    public roleService: RoleService
  ) {}

  ngOnInit() {
    this.initialFormCreateUser();
    this.fetchData();
  }
  initialFormCreateUser() {
    this.registerUser = this.formBuilder.group({
      username: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      photo: new FormControl(''),
      role_id: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
    });
  }

  fetchData() {
    this.roleService.getRoles().subscribe((res) => {
      this.roles = res;
    });
  }

  onCreateUser() {
    this.userService.createUser(this.registerUser.value).subscribe((res) => {
      console.log(res);
      if (!res.Error) {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Usuario creado Correctamente',
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Error al crear usuario',
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  }
}
