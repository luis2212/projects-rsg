import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BoardComponent } from './boards/board/board.component';
import { ModalScrumComponent } from './boards/board/modal-scrum/modal-scrum.component';
import { BoardsComponent } from './boards/boards.component';
import { AuthGuard } from './core/guards/auth.guard';
import { NoAuthGuard } from './core/guards/no-auth.guard';
import { GroupComponent } from './group/group.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './PageNotFound/PageNotFound.component';
import { ProfileComponent } from './profile/profile.component';
import { RoleComponent } from './role/role.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [NoAuthGuard] },
  { path: 'roles', component: RoleComponent },
  { path: 'groups', component: GroupComponent },
  { path: 'users', component: UsersComponent },
  {
    path: 'boardpanel',
    component: BoardsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'board/:id', component: BoardComponent },
  { path: 'profile', component: ProfileComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
