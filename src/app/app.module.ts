import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardsComponent } from './boards/boards.component';
import { LoginComponent } from './login/login.component';

import { PageNotFoundComponent } from './PageNotFound/PageNotFound.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { SidebarComponent } from './header/sidebar/sidebar.component';
import { ModalCreateBoardComponent } from './boards/modal-create-board/modal-create-board.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersComponent } from './users/users.component';
import { GroupComponent } from './group/group.component';
import { ModalUpdateBoardComponent } from './boards/modal-update-board/modal-update-board.component';
import { ToastrModule } from 'ngx-toastr';
import { BoardComponent } from './boards/board/board.component';
import { ModalScrumComponent } from './boards/board/modal-scrum/modal-scrum.component';
import { ModalScrumUpdateShowComponent } from './boards/board/modal-scrum-update-show/modal-scrum-update-show.component';
import { ModalCreateUserComponent } from './users/modal-create-user/modal-create-user.component';
import { RoleComponent } from './role/role.component';
import { ModalEditUserComponent } from './users/modal-edit-user/modal-edit-user.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { HeaderComponent } from './header/header.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ChecklistComponent } from './boards/board/modal-scrum-update-show/checklist/checklist.component';
import { ColorTwitterModule } from 'ngx-color/twitter';
import { CredentialEditComponent } from './boards/board/credential-edit/credential-edit.component';
import { MettingEditComponent } from './boards/board/metting-edit/metting-edit.component';
import { TicketEditComponent } from './boards/board/ticket-edit/ticket-edit.component';
import { ProfileComponent } from './profile/profile.component';
import localEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';

registerLocaleData(localEs, 'es');
@NgModule({
  declarations: [
    AppComponent,
    BoardsComponent,
    BoardComponent,
    LoginComponent,
    PageNotFoundComponent,
    SidebarComponent,
    ModalCreateBoardComponent,
    ModalUpdateBoardComponent,
    UsersComponent,
    GroupComponent,
    ModalScrumComponent,
    ModalScrumUpdateShowComponent,
    ModalCreateUserComponent,
    RoleComponent,
    ModalEditUserComponent,
    HeaderComponent,
    ChecklistComponent,
    CredentialEditComponent,
    MettingEditComponent,
    TicketEditComponent,
    ProfileComponent,
  ],
  imports: [
    NgxUiLoaderModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatDialogModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TimepickerModule.forRoot(),
    TabsModule.forRoot(),
    ColorTwitterModule,
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
