import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  public user: any;
  public id_user: number;
  public favoriteBoards: any;
  public mettingsToUser: any;
  public ticketsToUser: any;
  public scrumToUser:any[] = [];
  public groupToUser: any;
  constructor(private profileService: ProfileService) {}

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.id_user = this.user.user.id_user;
    this.user = this.user.user;

    console.log(this.user);

    this.onGetBoardToUser();
    this.onGetFavoriteBoardToUser();
    this.onGetGroupToUser();
    this.onGetMettingToUser();
    this.onGetScrumCardToUser();
    this.onGetTicketToUser();
  }

  onGetBoardToUser() {
    this.profileService.getBoardToUser(this.id_user).subscribe((res) => {});
  }

  onGetFavoriteBoardToUser() {
    this.profileService
      .getFavoriteBoardToUser(this.id_user)
      .subscribe((res) => {
        this.favoriteBoards = res.Favorite_Boards;
      });
  }

  onGetGroupToUser() {
    this.profileService.getGroupToUser(this.id_user).subscribe((res) => {
      this.groupToUser = res.AllGroup;
      console.log(this.groupToUser)
    });
  }

  onGetMettingToUser() {
    this.profileService.getMettingsToUser(this.id_user).subscribe((res) => {
      this.mettingsToUser = res.AllMetting;
      console.log(this.mettingsToUser);
    });
  }

  onGetTicketToUser() {
    this.profileService.getTicketToUser(this.id_user).subscribe((res) => {
      this.ticketsToUser = res.AllTickets;
    });
  }

  onGetScrumCardToUser() {
    this.profileService.getScrumToUser(this.id_user).subscribe((res) => {
      this.scrumToUser = res.AllScrum_Card;
      console.log(this.scrumToUser);
    });
  }
}
