import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private _location: Location,
    private router: Router,
    public notificationsService: NotificationsService
  ) {}

  public user: any;
  public id_user: number;
  public isNotified: boolean = true;

  public notifications: any;
  @ViewChild('notification') notificationContainer: ElementRef;

  ngOnInit() {
    this.getNotifications();
  }

  getNotifications() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.id_user = this.user.user.id_user;
    this.notificationsService
      .getNotifications(this.id_user)
      .subscribe((res) => {
        this.notifications = res.Data;
      });
  }

  onBack() {
    this._location.back();
  }
  onHome() {
    this.router.navigateByUrl('/');
  }
  showNotifications() {
    if (this.isNotified == true) {
      this.isNotified = !this.isNotified;
      this.notificationContainer.nativeElement.style.display = 'none';
    } else {
      this.notificationContainer.nativeElement.style.display = 'block';
      this.isNotified = !this.isNotified;
    }
  }
  deleteAllNotifications() {
    this.notificationsService
      .deleteAllNotifications(this.id_user)
      .subscribe((data) => {
        this.getNotifications();
        console.log(data);
      });
  }
}
