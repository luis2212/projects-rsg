import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  public user: any = [];
  public username: string;
  public name: string;
  public rol: string;
  public lastname: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }
  @Input('containId')
  public isBoards = true;
  public isBoard = false;
  ngOnInit() {
    this.username = this.user.user.username;
    this.name = this.user.user.name;
    this.lastname = this.user.user.lastname;
    this.rol = this.user.user.role.role_name;
  }

  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/');
  }

  onHome() {
    this.router.navigateByUrl('/');
  }
  onUsers() {
    this.router.navigateByUrl('/users');
  }
  onGroups() {
    this.router.navigateByUrl('/groups');
  }
  onRoles() {
    this.router.navigateByUrl('/roles');
  }
  onProfile() {
    this.router.navigateByUrl('/profile');
  }
}
