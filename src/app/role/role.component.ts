import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PrivilegeGroup } from '../interfaces/privilege_groups.interface';
import { RoleService } from './role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
})
export class RoleComponent implements OnInit {
  constructor(
    private readonly roleService: RoleService,
    private ngxLoader: NgxUiLoaderService) { }
  public roles: any;
  public permissions: any;
  public privileges: any;
  public privilegesGeneral: [];
  public privilege_groups: PrivilegeGroup[];

  public permisos_generales: any[] = [];
  public permisos_admin: any[] = [];
  public permisos_proyecto: any[] = [];
  public permisos_desarrollo: any[] = [];
  public permisos_soporte: any[] = [];

  public privileges_admin: any[] = [];
  public privileges_cliente: any[] = [];

  ngOnInit() {
    this.ngxLoader.start();
    this.getRoles();
    this.getPermissions();
    this.getPrivileges();
    this.getPrivilegesGroup();
  }

  getRoles() {
    this.roleService.getRoles().subscribe((roles) => {
      this.roles = roles;
    });
  }
  getPermissions() {
    this.roleService.getPermissions().subscribe((permissions) => {
      this.permissions = permissions;
      console.log(this.permissions);
      permissions.forEach((privi: any) => {
        console.log(privi.role_id.role_id);
        if (privi.role_id) {
          if (privi.role_id.role_id == 1) {
            this.privileges_admin.push(privi);
          }
          else {
            if (privi.role_id.role_id == 2) {
              this.privileges_cliente.push(privi);
            }
          }
        }
      })

    })
  }
  getPrivileges() {
    this.roleService.getPrivileges().subscribe((privileges) => {
      this.privileges = privileges;
      privileges.forEach((priv: any)=>{
        if(priv.privilege_groups.privig_position == 1){
          this.permisos_generales.push(priv);
        } else if(priv.privilege_groups.privig_position == 2){
          this.permisos_admin.push(priv);
        } else if(priv.privilege_groups.privig_position == 3){
          this.permisos_proyecto.push(priv);
        } else if(priv.privilege_groups.privig_position == 4) {
          this.permisos_desarrollo.push(priv);
        } else if(priv.privilege_groups.privig_position == 5) {
          this.permisos_soporte.push(priv);
        }
      })
      this.ngxLoader.stop();
    })
  }
  getPrivilegesGroup() {
    this.roleService.getPrivilegesGroup().subscribe((privilegesGroup) => {
      this.privilege_groups = privilegesGroup;
      
    })
  }
}
