import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from '../interfaces/role.interface';
import { environment } from '../../environments/environment';
import { Permission } from '../interfaces/permission.interface';
import { Privileges } from '../interfaces/privileges.interface';
import { PrivilegeGroup } from '../interfaces/privilege_groups.interface';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
public url = environment.baseUrl ;
constructor(private http: HttpClient) { }

  getRoles():Observable<Role>{
    return this.http.get<Role>(this.url+ 'role');
  }
  getPermissions():Observable<Permission[]>{
    return this.http.get<Permission[]>(this.url+ 'permissions');
  }
  getPrivileges():Observable<Privileges[]>{
    return this.http.get<Privileges[]>(this.url+ 'privileges');
  }
  getPrivilegesGroup():Observable<PrivilegeGroup[]>{
    return this.http.get<PrivilegeGroup[]>(this.url+ 'privilege_groups');
  }
  
}

