export interface DataGroup{
  datagroup_id: number;
  datagroup_name: string;
  datagroup_photo:string;
  group:Array<any>;
}