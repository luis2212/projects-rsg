import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Group } from '../interfaces/group.interface';
import { Response } from '../interfaces/response.interface';
import { DataGroup } from './group.interface';

@Injectable({
  providedIn: 'root',
})

export class GroupService {
  url: string = environment.baseUrl + 'data_group/';
  url2: string = environment.baseUrl + 'group';
  constructor(private http: HttpClient) {}

  getDataGroup(): Observable<DataGroup> {
    return this.http.get<DataGroup>(this.url);
  }
  getDataGroupById(id_datagroup: number) {
    return this.http.get<any>(this.url + id_datagroup);
  }

  createDataGroup(dataGroup: DataGroup) {
    return this.http.post<Response>(this.url, dataGroup);
  }
  pushUsersToGroup(group: Group) {
    return this.http.post<Response>(this.url2, group);
  }

  
  deleteUsersFromGroup(dataDelete: Group) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: dataDelete
    };
    return this.http.delete<Response>(this.url2, options);
  }

  deleteDataGroup(id_datagroup: number) {
    return this.http.delete<Response>(this.url + id_datagroup);
  }
}
