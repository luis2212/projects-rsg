import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { getgroups } from 'process';
import Swal from 'sweetalert2';
import { UsersService } from '../users/users.service';
import { GroupService } from './group.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
})
export class GroupComponent implements OnInit {
  public newGroupForm: FormGroup;
  public registerUsersForm: FormGroup;
  public groups: any;
  public users: any;
  public membersGroup: any = [];
  public id_datagroup: number;
  public showAddGroup: boolean = false;

  constructor(
    private groupService: GroupService,
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private ngxLoader: NgxUiLoaderService
  ) {}

  ngOnInit() {
    this.ngxLoader.start();
    this.getGroups();
    this.getUsers();
    this.initForms();
  }

  populatedIdGroup(id_datagroup: number) {
    this.id_datagroup = id_datagroup;
    this.registerUsersForm.get('datagroup_id').setValue(id_datagroup);
    this.groupService.getDataGroupById(id_datagroup).subscribe((res) => {
      res.Data_Group.group.forEach((user: any) => {
        this.membersGroup.push(user.users);
      });
    });
  }
  limpiarMiembros() {
    this.membersGroup = [];
    this.getGroups();
    this.getUsers();
  }
  initForms() {
    this.newGroupForm = this.formBuilder.group({
      datagroup_name: new FormControl('', Validators.required),
      datagroup_photo: new FormControl('', Validators.required),
    });
    this.registerUsersForm = this.formBuilder.group({
      id_user: new FormControl('', Validators.required),
      datagroup_id: new FormControl('', Validators.required),
    });
  }
  getGroups() {
    this.groupService.getDataGroup().subscribe((res) => {
      this.groups = res;
      this.ngxLoader.stop();
    });
  }
  getUsers() {
    this.userService.getUsers().subscribe((res) => {
      this.users = res;
      console.log(res);
    });
  }
  oncreateGroup() {
    this.groupService
      .createDataGroup(this.newGroupForm.value)
      .subscribe((res) => {
        if (res.Status !== 201) {
          Swal.fire({
            position: 'top',
            icon: 'error',
            title: 'No se pudo crear el grupo',
            showConfirmButton: false,
            timer: 500,
          });
        } else {
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Grupo creado',
            showConfirmButton: false,
            timer: 500,
          });
          this.getGroups();
          this.showAddGroup = false;
        }
      });
  }
  addUsersToGroup() {
    this.groupService
      .pushUsersToGroup(this.registerUsersForm.value)
      .subscribe((res) => {
        if (res.Status !== 201) {
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'El usuario no se puede agregar',
            showConfirmButton: false,
            timer: 1000,
          });
        } else {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Miembro agregado correctamente',
            showConfirmButton: false,
            timer: 1000,
          });
          this.limpiarMiembros();
          this.populatedIdGroup(this.id_datagroup);
        }
      });
  }
  deleteUsers(id_datagroup: number, id_user: number) {
    const body = {
      datagroup_id: id_datagroup,
      id_user: id_user,
    };
    this.groupService.deleteUsersFromGroup(body).subscribe((res) => {
      if (res.Status !== 201) {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'El usuario no se puede eliminar',
          showConfirmButton: false,
          timer: 1000,
        });
      } else {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Usuario Eliminado',
          showConfirmButton: false,
          timer: 1000,
        });
        this.getGroups();
        this.getUsers();
      }
    });
  }
  onDeleteDataGroup(id_datagroup: number) {
    this.groupService.deleteDataGroup(id_datagroup).subscribe((res) => {
      Swal.fire({
        title: '¿Desea eliminar el grupo?',
        text: 'Se eliminara el grupo definitivamente!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
      }).then((result) => {
        if (result.isConfirmed) {
          if (res.Status !== 201) {
            Swal.fire({
              position: 'top',
              icon: 'error',
              title: 'No se pudo eliminar el grupo',
              showConfirmButton: false,
              timer: 1000,
            });
          } else {
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Grupo eliminado',
              showConfirmButton: false,
              timer: 1000,
            });
            this.getGroups();
          }
        }
      });
    });
  }

  onShowForm() {
    this.showAddGroup = !this.showAddGroup;
  }
}
