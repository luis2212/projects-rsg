import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormGroupName,
  FormBuilder,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  subbmited = false;

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder
  ) {}

  createLogin() {
    this.login = this.formBuilder.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }
  onSubmit() {
    this.loginService
      .loginUser(this.login.value)
      .subscribe((res) => {
        if(res.error){
          Swal.fire({
            position: 'top',
            icon: 'error',
            title: res.error,
            footer: res.message,
            showConfirmButton: false,
            timer: 1500
          })
        }else{
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Accesando...',
            showConfirmButton: false,
            timer: 1500
          })
        }
      });
  }

  ngOnInit() {
    this.createLogin();
  }
}
