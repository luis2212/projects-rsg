import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  baseUrl = environment.baseUrl;
  httpOptions = {};
  nameUserLS: string = 'currentUser';
  private currentUser: BehaviorSubject<any>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUser = new BehaviorSubject(
      JSON.parse(localStorage.getItem(this.nameUserLS))
    );
  }

  get getUser() {
    return this.currentUser.value ? true : false;
  }

  loginUser(data: {
    username: string;
    password: string;
  }): Observable<{
    user: any;
    token: string;
    error: string;
    message: string;
    statusCode: number;
  }> {
    const body = {
      username: data.username,
      password: data.password,
    };
    const res = { user: {}, token: '' };
    return this.http
      .post<{
        user: any;
        token: string;
        error: string;
        message: string;
        statusCode: number;
      }>(this.baseUrl + 'auth/singin', data, this.httpOptions)
      .pipe(
        map((r) => {
          res.user = r.user;
          res.token = r.token;

          if (res.user) {
            this.router.navigateByUrl('/boardpanel');
          }
          this.setUserToLocalStorage(JSON.stringify(res));
          this.currentUser.next(res);
          return res;
        }),
        catchError((e) => {
          return of(e.error);
        })
      );
  }

  private setUserToLocalStorage(user: any) {
    localStorage.setItem(this.nameUserLS, user);
  }

  logout() {
    localStorage.removeItem(this.nameUserLS);
    this.currentUser.next(null);
    this.router.navigateByUrl(environment.baseUrl + '/');
  }
}
