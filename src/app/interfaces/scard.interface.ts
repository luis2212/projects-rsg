export class Scard {
  id_scard: number;
  id_list:number;
  scard_tittle: string;
  image_cover: string;
  color: string;
  scard_description: string;
  start_date: Date;
  end_date: Date;
  scard_status: boolean;
  start_date_devop: Date;
  end_date_devop: Date;
  dedicated_time: string;
  archived_scard: boolean;
  labels: any[];
  links: any[];
  attachments: any[];
  keppers: any[];
  comments: any[];
  checklists: any[];
}
