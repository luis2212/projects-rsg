import {Scard} from '../interfaces/scard.interface'

export interface BoardList {
  id_list: number;
  list_tittle: string;
  archived_list: boolean;
  scards: Scard[];
}