export interface Response {
  Error: string;
  Message: string;
  Data: any;
  Status: number;
}