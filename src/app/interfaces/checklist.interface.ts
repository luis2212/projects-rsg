import { Users } from '../users/user.interface';
import { Task } from './task.interface';

export interface Checklist {
  id_checklist: number;
  checklist_name: string;
  user: Users;
  tasks: Task[];
  progress: any;
}
