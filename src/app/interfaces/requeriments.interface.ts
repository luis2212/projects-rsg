export interface Requerements {
  id_request: number;
  request_description: string;
  request_status: boolean;
}
