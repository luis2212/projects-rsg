import { Users } from "../users/user.interface";

export interface Tickets {
  id_ticket:number;
  ticket_tittle:string;
  color:string;
  ticket_description:string;
  ticket_status:boolean;
  customer_date:Date;
  resolution_date:Date;
  ticket_time: string;
  solution:string; 
  archived_ticket: boolean;
  attendees: Users[];
}