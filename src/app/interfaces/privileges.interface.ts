export interface Privileges {
  privi_id:number;
  privi_code:string;
  privi_name:string;
  privi_active:boolean;
  privilege_groups: any[];
}