import { Users } from '../users/user.interface';

export interface Task {
  id_task: number;
  task_description: string;
  task_status: boolean;
  scheduled_data: Date;
  start_date: Date;
  end_date: Date;
  task_time: string;
  task_filename: string;
  task_link: string;
  user_attend: Users;
  id_checklist: number;
}
