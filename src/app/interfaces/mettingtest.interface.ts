import { timestamp } from "rxjs/operators";
import { Users } from "../users/user.interface";

export interface MettingTest {
  archived_metting: boolean;
  attendees:Users[];
  description_metting: string;
  end_time: string;
  id_metting:number;
  memorandum: string;
  platform: string;
  scheduled_date: Date;
  start_time: string;
  title_metting: string;
}