import { Users } from '../users/user.interface';

export interface Credentials {
  credential_description: string;
  credential_filename: string;
  credential_links: string;
  credential_path: string;
  id_credential: number;
  user_access: Users[];
  comments: any;
  id_board: number;
}
