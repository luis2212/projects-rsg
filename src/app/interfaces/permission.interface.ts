export interface Permission {
  perm_id: number;
  role_id: number;
  privi_id:number;
  perm_active: boolean;
}